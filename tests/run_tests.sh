#!/bin/bash

# Automatic Inform Testing Platform
# Copyright (C) 2012 Jos� Manuel Ferrer Ortiz (jmfo1982 AT yahoo dot es)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3,
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

type colordiff >/dev/null 2>/dev/null && diff=colordiff || diff=diff

for source in *.inf; do
  sourcebase=`basename $source .inf`
  echo Running tests for source file: $source

  # Compile the source file for the current group of tests
  library=`inform $source /tmp/$sourcebase.z5 | tail -n 1 | head -c 10 | tr '[:upper:]' '[:lower:]'`
  if [ ! -f /tmp/$sourcebase.z5 ]; then
    echo Error compiling $source
    continue
  fi

  if [ "$library" = "informate!" ]; then
    RESTART=REINICIAR  # RESTART command
    YES=SI  # Confirmation to QUIT and RESTART commands
  else
    RESTART=RESTART
    YES=YES
  fi

  # Build the list of commands to be played, from all tests in this group
  rm -f /tmp/$sourcebase.rec
  for testinput in $sourcebase.*.in; do
    testbase=`basename $testinput .in`
    echo SCRIPT ON >>/tmp/$sourcebase.rec
    echo /tmp/$testbase.scr >>/tmp/$sourcebase.rec
    tail -n +2 $testinput >>/tmp/$sourcebase.rec
    lastchar=`tail -c 1 $testinput`
    if [ "$lastchar" != "\n" ]; then  # No new line at end of file
      echo >>/tmp/$sourcebase.rec
    fi
    echo SCRIPT OFF >>/tmp/$sourcebase.rec
    echo $RESTART >>/tmp/$sourcebase.rec
    echo $YES >>/tmp/$sourcebase.rec
  done
  echo QUIT >>/tmp/$sourcebase.rec
  echo $YES >>/tmp/$sourcebase.rec
  echo >>/tmp/$sourcebase.rec

  # Run the tests on Frotz
  frotz -s 0 -S 70 /tmp/$sourcebase.z5 </tmp/$sourcebase.rec >/dev/null
  rm -f /tmp/$sourcebase.rec /tmp/$sourcebase.z5

  # Check each test in this group (the tests that share this source file)
  for testoutput in $sourcebase.*.out; do
    testbase=`basename $testoutput .out`
    header=`head -n 1 $testbase.in`
    echo -n "� $header: "

    # Strip the non-needed lines from the expected result
    outputfile=/tmp/$testoutput  # File to store the expected test output
    if [ "$library" = "informate!" ]; then
      firstline=`expr \`grep -in -m 1 "ola informate" $testoutput | cut -d':' -f 1\` + 2`
    else
      firstline=1
    fi
    lastline=`grep -in -m 1 "^> *SCRIPT OFF" $testoutput | cut -d':' -f 1`
    numlines=`expr $lastline - $firstline`
    tail -n +$firstline $testoutput | head -n $numlines | tr -d '\r' >$outputfile

    # Strip the non-needed lines from the actual result
    if [ "$library" = "informate!" ]; then
      firstline=`expr \`grep -in -m 1 "ola informate" /tmp/$testbase.scr | cut -d':' -f 1\` + 2`
    else
      firstline=1
    fi
    lastline=`grep -in -m 1 "^> *SCRIPT OFF" /tmp/$testbase.scr | cut -d':' -f 1`
    numlines=`expr $lastline - $firstline`
    tail -n +$firstline /tmp/$testbase.scr | head -n $numlines | tr -d '\r' >/tmp/$testbase.tmp
    mv /tmp/$testbase.tmp /tmp/$testbase.scr

    # Evaluate the test results
    result=`$diff -u /tmp/$testbase.scr $outputfile`
    rm -f $outputfile /tmp/$testbase.scr
    if [ -n "$result" ]; then
      echo -e "\033[31mNOK\033[0m"
      echo "$result"
    else
      echo -e "\033[32mOK\033[0m"
    fi
  done
done